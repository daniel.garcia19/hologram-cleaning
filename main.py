import pandas as pd

df = pd.read_csv('./data/unmanaged-data/Hologram.csv', dtype={'ICCID': object})

var = df['Name'].value_counts()

df = df[df['Name'].isin(var[var>1].index)]

var = df['Name'].value_counts()

df.to_csv('./data/managed-data/Repeated-SN.csv', index=False)

print(var)

print('Success~')
